import { Component ,NgModule} from '@angular/core';
import {List} from '../models/list';


export class Hero {
  id: number;
  title: string;
  img: any;
  price: string;
  Quantity: number;
  description: string;
}

const HEROES: Hero[] = [
    {id:1,title:'Nail Paint',img:"../app/pictures/paint.jpg",price:'$45', Quantity:4,description:'jfdjahsdk'},
    {id:2,title:'Eye Liner',img:"../app/pictures/kajal.jpg",price:'$55',Quantity:5,description:'ffdasf'},
    {id:3,title:'Foundation',img:"../app/pictures/foundation.jpg",price:'$45', Quantity:4,description:'jfdjahsdk'},
    {id:4,title:'EYE SHADOW',img:"../app/pictures/eyeshadow.jpg",price:'$55',Quantity:5,description:'ffdasf'},
    {id:5,title:'FACE FOUNDATION',img:"../app/pictures/foundation.jpg",price:'$45', Quantity:4,description:'jfdjahsdk'},
   
    
    
];

@Component({
  selector: 'my-app',
   templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent {
  title = 'Products List';
  heroes = HEROES;
  selectedHero: Hero;
  list: List[]=[];
  completed: boolean;
  searchHero;
  hero;heroeCopy;
  onSelect(hero: Hero): void {
    this.selectedHero = hero;

  }delete(){
    var index = this.heroes.indexOf((this.selectedHero));
    this.heroes.splice(index,1);
    console.log(this.heroes)
  }
 /* search():void{
    let term = this.searchHero;
    this.hero = this.heroeCopy.filter(function(tag){
      return tag.title.indexOf((term))>=0;
    })
  }*/
}



/*export class Pagination {

   filteredItems : Hero[];
   pages : number = 4;
  pageSize : number = 5;
   pageNumber : number = 0;
   currentIndex : number = 1;
   items: Hero[];
   pagesIndex : Array<number>;
   pageStart : number = 1;
   inputName : string = '';

   constructor( ){
         this.filteredItems = HEROES;
       this.init();
   };
   init(){
         this.currentIndex = 1;
         this.pageStart = 1;
         this.pages = 4;

         this.pageNumber = parseInt(""+ (this.filteredItems.length / this.pageSize));
         if(this.filteredItems.length % this.pageSize != 0){
            this.pageNumber ++;
         }
    
         if(this.pageNumber  < this.pages){
               this.pages =  this.pageNumber;

         }
       
         this.refreshItems();
         console.log("this.pageNumber :  "+this.pageNumber);
   }

   FilterByName(){
      this.filteredItems = [];

      if(this.inputName != ""){
            HEROES.forEach(element => {
                if(element.title.toUpperCase().indexOf(this.inputName.toUpperCase())>=0){
                  this.filteredItems.push(element);
               }
            });
      }else{
         this.filteredItems = HEROES;
      }
      console.log(this.filteredItems);
      this.init();
   }
   fillArray(): any{
      var obj = new Array();
      for(var index = this.pageStart; index< this.pageStart + this.pages; index ++) {
                  obj.push(index);
      }
      return obj;
   }
 refreshItems(){
               this.items = this.filteredItems.slice((this.currentIndex - 1)*this.pageSize, (this.currentIndex) * this.pageSize);
               this.pagesIndex =  this.fillArray();

   }
   prevPage(){
      if(this.currentIndex>1){
         this.currentIndex --;
      } 
      if(this.currentIndex < this.pageStart){
         this.pageStart = this.currentIndex;

      }
      this.refreshItems();
   }
   nextPage(){
      if(this.currentIndex < this.pageNumber){
            this.currentIndex ++;
      }
      if(this.currentIndex >= (this.pageStart + this.pages)){
         this.pageStart = this.currentIndex - this.pages + 1;
      }
 
      this.refreshItems();
   }
    setPage(index : number){
         this.currentIndex = index;
         this.refreshItems();
    }

 }
*/
