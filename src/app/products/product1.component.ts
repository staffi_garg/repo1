import { Component } from '@angular/core';

import {List} from '../models/list';

export class Hero {
  id: number;
  title: string;
    img: any;
    price: string;
    Quantity: number;
    description: string;
}

const HEROES: Hero[] = [
    {id:6,title:'FACE COMPACT',img:"../app/pictures/foundation.jpg",price:'$55',Quantity:5,description:'ffdasf'},
    {id:7,title:'Lipstick',img:"../app/pictures/lipglose.jpg",price:'$45', Quantity:4,description:'jfdjahsdk'},
    {id:8,title:'Eye Liner',img:"../app/pictures/eyeliner.jpg",price:'$55',Quantity:5,description:'ffdasf'},
    {id:9,title:'Nail Paint',img:"../app/pictures/paint.jpg",price:'$45', Quantity:4,description:'jfdjahsdk'},
    {id:10,title:'Eye Liner',img:"../app/pictures/kajal.jpg",price:'$55',Quantity:5,description:'ffdasf'}, 
];

@Component({
  selector: 'my-app',
   templateUrl: './product1.component.html',
  styleUrls: ['./product1.component.css']
})
export class ProductComponent {
  title = 'Tour of Heroes';
  heroes = HEROES;
  selectedHero: Hero;
    list: List[]=[];
   completed: boolean;

  onSelect(hero: Hero): void {
    this.selectedHero = hero;

  }delete(){
    var index = this.heroes.indexOf((this.selectedHero));
    this.heroes.splice(index,1);
    console.log(this.heroes)
  }

}

/*import {Component} from '@angular/core';


@Component({
  selector: 'home',
  styleUrls: ['./product1.component.css'],
  templateUrl: './product1.component.html'
})
export class ProductComponent  {
	delete(){
		return confirm('Are you absolutely sure you want to delete?')
	}
	}*/