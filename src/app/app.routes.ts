import { Routes } from '@angular/router';

import { AboutComponent } from './about/about.component';
import {HomeComponent} from './home/home.component';
import {ContactComponent} from './contact/contact.component';
import { ProductsComponent     } from './products/products.component';
import {ProductComponent} from './products/product1.component';
/*import {AddProduct} from './addproducts/addproducts.component';
*/import {EditComponent} from './edit/edit.component';

export const rootRouterConfig: Routes = [
  { path: 'home',  pathMatch: 'full' , component: HomeComponent },
  
  { path: 'about', component: AboutComponent },
  { path: 'product', component: ProductsComponent },
  { path: 'contact', component: ContactComponent },
  {path: 'product1', component: ProductComponent},
  {path: 'edit', component: EditComponent},
 /* {path: 'addProduct', component: AddProduct},*/
];

