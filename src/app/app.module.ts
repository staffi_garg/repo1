import { NgModule             } from '@angular/core'
import { RouterModule         } from '@angular/router';
import { rootRouterConfig     } from './app.routes';
import { AppComponent         } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule        } from '@angular/platform-browser';
import { HttpModule           } from '@angular/http';
import { userService          } from '../app/shared/users.service';
import { AboutComponent       } from './about/about.component';
import { ProductsComponent     } from './products/products.component';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import {HomeComponent} from './home/home.component';
import {ContactComponent} from './contact/contact.component';
import { ProductComponent} from './products/product1.component';
import {EditComponent} from './edit/edit.component';
import {Ng2PaginationModule} from 'ng2-pagination';

/*import {AddProduct} from './addproducts/addproducts.component';

*/
/*import { Pagination} from './products/products.component';*/
@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    ProductComponent,
    HomeComponent,
    ContactComponent,
    ProductsComponent,
    EditComponent,
    
   /* AddProduct*/
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,Ng2PaginationModule,
    HttpModule,
    RouterModule.forRoot(rootRouterConfig, { useHash: true })
  ],
  providers: [
     userService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {

}
