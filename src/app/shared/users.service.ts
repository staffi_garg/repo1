import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Products} from '../models/users';

@Injectable()
export class userService {
	
	constructor(private http : Http){}
    product_list=[]


    updateProduct(product){
        this.product_list.push(product)
    }

    getProducts(){
        return this.product_list
    }
	

private handleError(err) {
        let errMessage: string;

        if (err instanceof Response) {
            let body = err.json() || '';
            let error = body.error || JSON.stringify(body);
            errMessage = `${err.status} - ${err.statusText} || ''} ${error}`;
        } else {
            errMessage = err.message ? err.message : err.toString();
        }

        return Observable.throw(errMessage);
    }

}


