import {Component} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import {FormControl} from '@angular/forms';
import {ActivatedRoute,Router} from '@angular/router';
import {userService} from '../shared/users.service';
import {Products} from '../models/users';

@Component({
  selector: 'home',
  styleUrls: ['./edit.component.css'],
  templateUrl: './edit.component.html'
})
export class EditComponent  {
	 DataList5: any = [];
	 user: Products;
	constructor(private router: Router,private formBuilder: FormBuilder, private user_service: userService) {
		console.log(this.user_service.getProducts())
	}
	Products: any={
		img:'',
		title:'',
		price: '',
		Quantity:'',
		description:'',
	};
	submitData(title,price,Quantity,description){
		
		console.log(this.Products)
		this.user_service.updateProduct(this.Products)
		this.Products="";
		
	}
}