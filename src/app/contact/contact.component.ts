import { Component} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import {FormControl} from '@angular/forms';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact-component.css']
})
export class ContactComponent  {
 
  constructor(private formBuilder: FormBuilder) {}

  
    data : any={
      name: '',
      email: '', 
      content: '', 
    };
  
  submitForm() {
   
    console.log(this.data);
    this.data="";
  }
}